const puppeteer = require('puppeteer');
    async function main() {
        const browser = await puppeteer.launch({
            headless: false
        });
        const page = await browser.newPage();
        await page.goto('https://zeeland.nazca4u.nl/rapportage/AddressLookUp/AddressLookUp.aspx');
        await page.type('#ContentPlaceHolder1_txtStreet', 'Hans Lipperheystraat');
        await page.type('#ContentPlaceHolder1_txtHouseNumber', '25');
        await page.type('#ContentPlaceHolder1_txtZipCode', '4336DB');
        await page.type('#ContentPlaceHolder1_txtCity', 'Middelburg');
        await page.click('#ContentPlaceHolder1_btnSearch');
        await page.screenshot({ path: 'example4.png' });
        await browser.close();
    }
    main();