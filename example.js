    const puppeteer = require('puppeteer');
    async function main() {
        const browser = await puppeteer.launch({
            headless: false
        });
        const page = await browser.newPage();
        await page.goto('https://zeeland.nazca4u.nl/Rapportage/viewerLookUp/Geolocator.aspx?showzoeker=1');
        await page.$eval( '#anchor-open', form => form.click() );
        await page.waitForSelector('#cphContent_txtStreet');
        await page.type('#cphContent_txtStreet', 'Hans Lipperheystraat', {delay: 100});
        await page.waitForTimeout(5000); // wait for 5 seconds
        await page.screenshot({ path: 'example.png' });
        await browser.close();
    }
    main();