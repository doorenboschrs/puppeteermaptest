   const puppeteer = require('puppeteer');
    async function main() {
        const browser = await puppeteer.launch({
            headless: false
        });
        const page = await browser.newPage();
        await page.goto('https://zeeland.nazca4u.nl/rapportage/viewerLookUp/Geolocator.aspx?showzoeker=1');
        //await page.waitForTimeout(9000); // wait for 5 seconds
        await page.type('#cphContent_txtStreet', 'Hans Lipperheystraat');
        await page.type('#cphContent_txtHouseNumber', '25');
        await page.type('#cphContent_txtZipCode', '4336DB');
        await page.type('#cphContent_txtCity', 'Middelburg');
        await page.click('#cphContent_btnSearch');
        await page.waitForTimeout(5000); // wait for 5 seconds
        await page.screenshot({ path: 'example.png' });
        await browser.close();
    }
    main();