/** require dependencies */
const express = require("express")
const cors = require('cors')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const path = require('path')
const puppeteer = require("puppeteer")

const app = express()

let port = process.env.PORT || 5000

/** set up middlewares */
app.use(cors())
app.use(bodyParser.json({limit: '50mb'}))
app.use(helmet())

app.get("/", (request, response) => {
    response.sendFile(path.join(__dirname, 'index.html'));
});

app.get("/style.css", (request, response) => {
    response.sendFile(path.join(__dirname, 'style.css'));
});

app.get('*', function(req, res) {
  res.sendFile(path.resolve(__dirname, 'index.html'));
});

app.use('/static',express.static(path.join(__dirname,'static')))
app.use('/assets',express.static(path.join(__dirname,'assets')))

app.post('/api/screenshot', (req, res, next) => {
    const { url } = req.body

    var screenshot = takeScreenshot(url)
    res.send({result: screenshot })
    next()    
})

async function takeScreenshot(url) {
    const browser = await puppeteer.launch({ 
       headless: true,
       args: ['--no-sandbox'] 
     });
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle0' });
    const screenShot = await page.screenshot({
        path: "./",
        type: "png",
        fullPage: true
    })

    await browser.close();
    return screenshot;
}

/** start server */
app.listen(port, () => {
    console.log(`Server started at port: ${port}`);
});
